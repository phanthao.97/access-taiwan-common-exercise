<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('github_users', function (Blueprint $table) {
            $table->id();
            $table->string('login', 255);
            $table->string('node_id', 255);
            $table->string('avatar_url', 255)->nullable();
            $table->string('gravatar_id', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->string('html_url', 255)->nullable();
            $table->string('followers_url', 255)->nullable();
            $table->string('following_url', 255)->nullable();
            $table->string('gists_url', 255)->nullable();
            $table->string('starred_url', 255)->nullable();
            $table->string('subscriptions_url', 255)->nullable();
            $table->string('organizations_url', 255)->nullable();
            $table->string('repos_url', 255)->nullable();
            $table->string('events_url', 255)->nullable();
            $table->string('received_events_url', 255)->nullable();
            $table->string('type', 255)->nullable();
            $table->boolean('site_admin')->default(false)->nullable();
            $table->string('name', 255);
            $table->string('company', 255)->nullable();
            $table->string('blog', 255)->nullable();
            $table->string('location', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('hireable', 255)->nullable();
            $table->string('bio', 255)->nullable();
            $table->string('twitter_username', 255)->nullable();
            $table->string('public_repos', 255)->nullable();
            $table->string('public_gists', 255)->nullable();
            $table->string('followers', 255)->nullable();
            $table->string('following', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('github_users');
    }
};

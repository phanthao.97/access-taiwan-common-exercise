<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\GithubUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    /**
     * Get list user from github
     *
     * @return json
     */
    public function getListUser()
    {
        $gitUrl = config('github.github_url');
        $response = Http::withHeaders(
            [
                'Accept' => 'application/vnd.github+json',
            ]
        )
            ->get("{$gitUrl}/users?per_page=100");

        return response()->json(
            [
                'data' => $response->object(),
                'total' => count($response->object())
            ]
        );
    }

    /**
     * Get user from github
     * 
     * @param $userName user login name
     *
     * @return json
     */
    public function getUser($userName)
    {
        $githubUser = GithubUser::where('login', $userName)->first();
        if ($githubUser) {
            $githubUser->site_admin = $githubUser->site_admin === 0 ? false : true;
            return response()->json($githubUser);
        }
        $gitUrl = config('github.github_url');
        $response = Http::withHeaders(
            [
                'Accept' => 'application/vnd.github+json',
            ]
        )
            ->get("{$gitUrl}/users/{$userName}");

        return response()->json($response->object());
    }

    /**
     * Update user to DB
     * 
     * @param $user User need to create or update
     *
     * @return json
     */
    public function updateUser(Request $request)
    {
        $user = $request->user;
        $responseMessage = "Record updated successfully";
        $res = GithubUser::updateOrCreate(
            ['login' => $user['login']],
            $user
        );

        if ($res) {
            return response()->json(
                [
                    'message' => $responseMessage,
                    'status' => 'Successful'
                ]
            );
        }
        $responseMessage = "Can't update!!!";
        return response()->json(
            [
                'message' => $responseMessage,
                'status' => 'Failure'
            ]
        );
    }
}

## About Application

This application shows the GitHub users in a List and allows user can search ,edit it and save to Database.

Backend: Laravel 9.39.0
Frontend: Vue 3.2.37

## Build Application

Require:

-   Laravel 9.x requires a minimum PHP version of 8.0.
-   MySql
-   Composer
-   Node.js version 16.0 or higher

Build Backend:
Create DB and config in ".env" file.
Run follow command:

-   Composer install
-   php artisan migrate

Build Frontend:
Run follow command:

-   cd .\vue\ and setup your api url in file .env (dev mode or prod mode)
-   npm install
-   npm run dev (for development mode) || npm run build (for production mode)

import request from "../utils/request";

export function getListUser() {
  return request({
    url: "/users",
    method: "get",
  });
}

export function getUser(username) {
  return request({
    url: `/user/${username}`,
    method: "get",
  });
}

export function updateUser(user) {
  return request({
    url: `/user`,
    method: "post",
    data: { 'user': user },
  });
}

import { getListUser, getUser, updateUser } from "../../api/users";

const state = {
  list: {},
};

const actions = {
  getListUser({ commit }) {
    return new Promise((resolve, reject) => {
      getListUser()
        .then((response) => {
          commit("setListUser", response.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  getUser({ commit }, username) {
    return new Promise((resolve, reject) => {
      getUser(username)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  updateUser({ commit }, user) {
    return new Promise((resolve, reject) => {
      updateUser(user)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  setListUser: (state, users) => {
    state.list = users;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

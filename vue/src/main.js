import { createApp } from "vue";
import "./style.css";
import store from "./store";
import router from "./router";
import "./index.css";
import App from "./App.vue";
import toast from "@k90mirzaei/vue-toast"
import '@k90mirzaei/vue-toast/dist/index.css'

createApp(App)
  .use(store)
  .use(router)
  .use(toast)
  .mount("#app");

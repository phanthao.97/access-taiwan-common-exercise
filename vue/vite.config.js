import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import * as dotenv from "dotenv";

export default ({ mode }) => {
  dotenv.config({ path: `./.env.${mode}`, override: true });
  // now you can access config with process.env.{configName}

  return defineConfig({
    plugins: [vue()],
  });
};
